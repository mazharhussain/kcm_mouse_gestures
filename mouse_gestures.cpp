/**
 * SPDX-FileCopyrightText: 2023 Mazhar Hussain <realmazharhussain@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "mouse_gestures.h"

#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON (MouseGestures, "kcm_mouse_gestures.json")

MouseGestures::MouseGestures(QObject *parent, const KPluginMetaData &data, const QVariantList &args)
    : KQuickAddons::ManagedConfigModule (parent, data, args)
{
    setButtons (Help | Apply | Default);
}

#include "mouse_gestures.moc"
