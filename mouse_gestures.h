/**
 * SPDX-FileCopyrightText: 2023 Mazhar Hussain <realmazharhussain@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include <KQuickAddons/ManagedConfigModule>

class MouseGestures : public KQuickAddons::ManagedConfigModule {
    Q_OBJECT

public:
    MouseGestures(QObject *parent, const KPluginMetaData &data, const QVariantList &args);
};
