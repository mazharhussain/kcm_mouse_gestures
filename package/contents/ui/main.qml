/**
 * SPDX-FileCopyrightText: 2023 Mazhar Hussain <realmazharhussain@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls

import org.kde.kirigami 2.7 as Kirigami
import org.kde.kcm 1.2

SimpleKCM {
    Controls.Label {
        text: "Hello World"
    }
}
